#pragma once

#include <string>

class Beduerfnis
{
public:
	Beduerfnis(std::string Name, int id, float wachstumProTick, float schwellwert);
	~Beduerfnis();

	int ID();
	void AendereWert(float aenderung);
	void Tick();
	float GetWert();
	float GetSchwelle();
	float GetWachstum();
	std::string GetName();
	bool GetDringend();

private:
	int m_InstanceID;
	float m_AktuellerWert;
	float m_Schwellwert;
	float m_Wachstum;
	std::string m_Name;
};

