#pragma once

#include <string>
#include <map>

class Aktion
{
public:
	Aktion(std::string name, int dauerInTicks);
	~Aktion();

	int ID();
	int GetDauer();
	std::string GetName();
	void SetEffekt(float effekt, int beduerfnisID);
	float GetEffekt(int beduerfnisID);

private:
	static int m_globalIDcounter;
	int m_InstanzID;
	std::map<int, float> m_EffektProBeduerfnis;
	std::string m_Name;
	int m_DauerInTicks;
};

