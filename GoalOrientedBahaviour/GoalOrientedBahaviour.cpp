// GoalOrientedBahaviour.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//

#include "pch.h"

#include "Beduerfnis.h"
#include "Aktion.h"
#include "GOBEngine.h"

#include <iostream>
#include <string>
using namespace std;


int main()
{
	Beduerfnis essen("Essen", GOBEngine::essen, 0.1f, 0.85f);
	Beduerfnis trinken("Trinken", GOBEngine::trinken, 0.05f, 0.6f);
	Beduerfnis ausscheiden("Ausscheiden", GOBEngine::ausscheiden, 0.05f, 0.8f);

	Aktion Bratwurst("Bratwurststand", 2);
	Bratwurst.SetEffekt(-1.0f, GOBEngine::essen);
	Bratwurst.SetEffekt(+0.2f, GOBEngine::trinken);
	Bratwurst.SetEffekt(+0.2, GOBEngine::ausscheiden);

	Aktion Restaurant("Restaurant", 4);
	Restaurant.SetEffekt(-1.0f, GOBEngine::essen);
	Restaurant.SetEffekt(-1.0f, GOBEngine::trinken);
	Restaurant.SetEffekt(-0.2f, GOBEngine::ausscheiden);

	Aktion Brunnen("Trinke von Brunnen",1);
	Brunnen.SetEffekt(+0.1f, GOBEngine::essen);
	Brunnen.SetEffekt(-1.0f, GOBEngine::trinken);
	Brunnen.SetEffekt(+0.1f, GOBEngine::ausscheiden);

	Aktion DixiKlo("Dixi-Klo", 2);
	DixiKlo.SetEffekt(-1.0f, GOBEngine::ausscheiden);

	GOBEngine engine;
	engine.AddBeduerfnis(&essen);
	engine.AddBeduerfnis(&trinken);
	engine.AddBeduerfnis(&ausscheiden);

	engine.AddAktion(&Bratwurst);
	engine.AddAktion(&Restaurant);
	engine.AddAktion(&Brunnen);
	engine.AddAktion(&DixiKlo);

	while (true) {
		engine.Tick(GOBEngine::GESAMT_ZEIT);
		system("PAUSE");
	}
}
