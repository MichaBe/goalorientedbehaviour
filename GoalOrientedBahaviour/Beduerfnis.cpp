#include "pch.h"
#include "Beduerfnis.h"

Beduerfnis::Beduerfnis(std::string name, int id, float wachstumProTick, float schwellwert)
{
	m_InstanceID = id;
	m_AktuellerWert = 0.0f;
	m_Schwellwert = schwellwert;
	m_Wachstum = wachstumProTick;
	m_Name = name;
}

Beduerfnis::~Beduerfnis()
{
}

int Beduerfnis::ID()
{
	return m_InstanceID;
}

void Beduerfnis::AendereWert(float aenderung)
{
	m_AktuellerWert += aenderung;
}

void Beduerfnis::Tick()
{
	m_AktuellerWert += m_Wachstum;
}

float Beduerfnis::GetWert()
{
	return m_AktuellerWert;
}

float Beduerfnis::GetSchwelle()
{
	return m_Schwellwert;
}

float Beduerfnis::GetWachstum()
{
	return m_Wachstum;
}

std::string Beduerfnis::GetName()
{
	return std::string();
}

bool Beduerfnis::GetDringend()
{
	return (m_AktuellerWert >= m_Schwellwert);
}
