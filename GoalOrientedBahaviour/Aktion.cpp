#include "pch.h"
#include "Aktion.h"

int Aktion::m_globalIDcounter = 0;

Aktion::Aktion(std::string name, int dauerInTicks)
{
	m_InstanzID = m_globalIDcounter++;
	m_Name = name;
	m_DauerInTicks = dauerInTicks;
}

Aktion::~Aktion()
{
}

int Aktion::ID()
{
	return m_InstanzID;
}

int Aktion::GetDauer()
{
	return m_DauerInTicks;
}

std::string Aktion::GetName()
{
	return m_Name;
}

void Aktion::SetEffekt(float effekt, int beduerfnisID)
{
	m_EffektProBeduerfnis[beduerfnisID] = effekt;
}

float Aktion::GetEffekt(int beduerfnisID)
{
	float returner = 0.0f;
	if (m_EffektProBeduerfnis.count(beduerfnisID) != 0)
		returner = m_EffektProBeduerfnis.at(beduerfnisID);
	return returner;
}
