#pragma once

#include "Aktion.h"
#include "Beduerfnis.h"

#define NOMINMAX
#include <Windows.h>
#include <iostream>
#include <map>
#include <list>
#include <algorithm>

class GOBEngine
{
public:
	enum BeduerfnisIDs {essen, trinken, ausscheiden};
	enum Strategie {SIMPLE, GESAMT, GESAMT_ZEIT};
	GOBEngine();
	~GOBEngine();
	
	void AddBeduerfnis(Beduerfnis* b);
	void AddAktion(Aktion* a);
	void DeleteAktion(Aktion* a);
	void Tick(Strategie s);

private:
	std::list<Beduerfnis*> m_Beduerfnisse;
	std::map<int, Aktion*> m_Aktionen;
	
	std::list<Beduerfnis*> m_AktuelleBeduerfnisse;
	std::list<Aktion*> m_TODO;
	int m_TicksSeitStartAktuellerAktion;

	void SimpleTick();
	void GesamtTick();
	void GesamtZeitTick();

	void SchreibeErgebnis();
	void ClearConsole();
};

