#include "pch.h"
#include "GOBEngine.h"


GOBEngine::GOBEngine()
{
	m_TicksSeitStartAktuellerAktion;
}

GOBEngine::~GOBEngine()
{
}

void GOBEngine::AddBeduerfnis(Beduerfnis * b)
{
	m_Beduerfnisse.push_back(b);
}

void GOBEngine::AddAktion(Aktion * a)
{
	m_Aktionen[a->ID()] = a;
}

void GOBEngine::DeleteAktion(Aktion * a)
{
	m_Aktionen.erase(a->ID());
}

void GOBEngine::Tick(Strategie s)
{
	ClearConsole();
	switch (s) {
	case SIMPLE:
		SimpleTick();
		break;
	case GESAMT:
		GesamtTick();
		break;
	case GESAMT_ZEIT:
		GesamtZeitTick();
		break;
	}
	SchreibeErgebnis();
}

void GOBEngine::SimpleTick()
{
	if (m_TODO.size() != 0) {
		Aktion* aktion = m_TODO.front();
		if (aktion->GetDauer() <= m_TicksSeitStartAktuellerAktion) {
			//Aktion ausf�hren
			for (auto b : m_Beduerfnisse) {
				b->AendereWert(aktion->GetEffekt(b->ID()));
			}
			m_TODO.clear();
			m_TicksSeitStartAktuellerAktion = 0;
		}
	}
	if (m_TODO.size() == 0) {
		Beduerfnis* bed = nullptr;
		for (auto b : m_Beduerfnisse) {
			if (b->GetWert() >= b->GetSchwelle()) { //Erstes bed�rfnis �ber schwelle ausw�hlen
				bed = b;
				break;
			}
		}
		if (bed != nullptr) {
			Aktion* next = m_Aktionen.begin()->second;
			for (auto it : m_Aktionen) {
				if (it.second->GetEffekt(bed->ID()) < next->GetEffekt(bed->ID())) { //Aktion mit st�rkstem effekt ausw�hlen
					next = it.second;
				}
			}
			m_TODO.push_back(next);
			m_TicksSeitStartAktuellerAktion = 0;
		}
	}
	for (auto it : m_Beduerfnisse) {
		it->Tick();
	}
	m_TicksSeitStartAktuellerAktion++;
}

void GOBEngine::GesamtTick()
{
	if (m_TODO.size() != 0) {
		Aktion* aktion = m_TODO.front();
		if (aktion->GetDauer() <= m_TicksSeitStartAktuellerAktion) {
			//Aktion ausf�hren
			for (auto b : m_Beduerfnisse) {
				b->AendereWert(aktion->GetEffekt(b->ID()));
			}
			m_TODO.clear();
			m_TicksSeitStartAktuellerAktion = 0;
		}
	}
	if (m_TODO.size() == 0) {
		m_AktuelleBeduerfnisse.clear();
		for (auto b : m_Beduerfnisse) {
			if (b->GetWert() >= b->GetSchwelle()) { //Alle Bed�rfnisse �ber schwelle ausw�hlen
				m_AktuelleBeduerfnisse.push_back(b);
				break;
			}
		}
		if (m_AktuelleBeduerfnisse.size() != 0) {
			Aktion* next = m_Aktionen.begin()->second;
			float nextUnzufriedenheit = 100.0f;
			
			for (auto itAktion : m_Aktionen) {
				float unzufriedenheitDieserAktion = 0.0f;
				for (auto itBeduerfnis : m_AktuelleBeduerfnisse) { //Gesamt-Unzufriedenheit berechnen
					float effektAufDiesesBed = (itBeduerfnis->GetWert() + itAktion.second->GetEffekt(itBeduerfnis->ID()));
					effektAufDiesesBed = std::max(0.0f, effektAufDiesesBed);
					unzufriedenheitDieserAktion = unzufriedenheitDieserAktion+effektAufDiesesBed;
				}
				if (unzufriedenheitDieserAktion < nextUnzufriedenheit) {
					next = itAktion.second;
					nextUnzufriedenheit = unzufriedenheitDieserAktion;
				}
			}

			m_TODO.push_back(next);
			m_TicksSeitStartAktuellerAktion = 0;
		}
	}
	for (auto it : m_Beduerfnisse) {
		it->Tick();
	}
	m_TicksSeitStartAktuellerAktion++;
}

void GOBEngine::GesamtZeitTick()
{
	if (m_TODO.size() != 0) {
		Aktion* aktion = m_TODO.front();
		if (aktion->GetDauer() <= m_TicksSeitStartAktuellerAktion) {
			//Aktion ausf�hren
			for (auto b : m_Beduerfnisse) {
				b->AendereWert(aktion->GetEffekt(b->ID()));
			}
			m_TODO.clear();
			m_TicksSeitStartAktuellerAktion = 0;
		}
	}
	if (m_TODO.size() == 0) {
		m_AktuelleBeduerfnisse.clear();
		for (auto b : m_Beduerfnisse) {
			if (b->GetWert() >= b->GetSchwelle()) { //Alle Bed�rfnisse �ber schwelle ausw�hlen
				m_AktuelleBeduerfnisse.push_back(b);
				break;
			}
		}
		if (m_AktuelleBeduerfnisse.size() != 0) {
			Aktion* next = m_Aktionen.begin()->second;
			float nextUnzufriedenheit = 100.0f;

			for (auto itAktion : m_Aktionen) {
				float unzufriedenheitDieserAktion = 0.0f;
				int iTick = itAktion.second->GetDauer();
				for (auto itBeduerfnis : m_AktuelleBeduerfnisse) { //Gesamt-Unzufriedenheit berechnen
					float effektAufDiesesBed = (itBeduerfnis->GetWert() + itAktion.second->GetEffekt(itBeduerfnis->ID()));
					effektAufDiesesBed += (itBeduerfnis->GetWachstum()*iTick);//Zeit auch ber�cksichtigen!
					effektAufDiesesBed = std::max(0.0f, effektAufDiesesBed);
					unzufriedenheitDieserAktion = unzufriedenheitDieserAktion + effektAufDiesesBed;
				}
				if (unzufriedenheitDieserAktion < nextUnzufriedenheit) {
					next = itAktion.second;
					nextUnzufriedenheit = unzufriedenheitDieserAktion;
				}
			}

			m_TODO.push_back(next);
			m_TicksSeitStartAktuellerAktion = 0;
		}
	}
	for (auto it : m_Beduerfnisse) {
		it->Tick();
	}
	m_TicksSeitStartAktuellerAktion++;
}

void GOBEngine::SchreibeErgebnis()
{
	std::cout << "Aktuelle Aktion: ";
	if (m_TODO.size() > 0) {
		std::cout << m_TODO.front()->GetName();
		std::cout << "(Tick " << m_TicksSeitStartAktuellerAktion << "/" << m_TODO.front()->GetDauer() <<")";
	}
	std::cout << std::endl << std::endl;

	std::cout << "Beduerfnisse: " << std::endl;
	for (auto b : m_Beduerfnisse) {
		std::cout << b->GetName() << " (" << b->GetWert() <<"/"<<b->GetSchwelle() << ", +" <<b->GetWachstum()<<")"<< std::endl;
		int anz = b->GetWert() * 40;
		char fuellung = '#';
		if (b->GetWert() >= b->GetSchwelle())
			fuellung = '!';
		for (int i = 0; i < anz; ++i) {
			std::cout << fuellung;
		}
		std::cout << std::endl << std::endl;
	}
}

void GOBEngine::ClearConsole()
{
	COORD start = { 0,0 };
	CONSOLE_SCREEN_BUFFER_INFO s;
	HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleScreenBufferInfo(console, &s);
	DWORD written, cells = s.dwSize.X * s.dwSize.Y;
	FillConsoleOutputCharacter(console, ' ', cells, start, &written);
	FillConsoleOutputAttribute(console, s.wAttributes, cells, start, &written);
	SetConsoleCursorPosition(console, start);
}
